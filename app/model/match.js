var mongoose = require('mongoose');

module.exports = mongoose.model('Match', {
    cl: {
        type: String,
        default: ''
    },
    ci: {
        type: String,
        default: ''
    },
    na: {
        type: String,
        default: ''
    },
    sm: {
        type: String,
        default: ''
    },
    cb: {
        type: String,
        default: ''
    },
    c1: {
        type: String,
        default: ''
    },
    c2: {
        type: String,
        default: ''
    },
    c3: {
        type: String,
        default: ''
    },
    t1: {
        type: String,
        default: ''
    },
    t2: {
        type: String,
        default: ''
    },
    t3: {
        type: String,
        default: ''
    },
    created: {
        type: Date,
        default: Date.now()
    }
});