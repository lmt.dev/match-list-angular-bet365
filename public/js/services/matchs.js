angular.module('matchService', [])

	.factory('Matchs', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/matchs');
			},
		}
	}]);